@extends('base')
@section('content')
    <div class="container">
        <form action="{{route('posts.update', $post->id)}}" method="post">
            @method('PUT')
            @csrf
            <label for="title">Título</label>
            <input type="text" name="title" id="title" class="form-control" value="{{$post->title}}">
            <label for="description">Descrição</label>
            <input type="text" name="description" id="description" class="form-control" value="{{$post->description}}">
            <label for="content">Conteúdo</label>
            <textarea name="content" id="content" class="form-control">{{$post->content}}</textarea>
            <br>
            <input type="submit" value="Salvar" class="btn btn-primary" style="float: right">
        </form>
    </div>
@endsection
