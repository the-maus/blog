@extends('base')
@section('content')
<div class="container">
    <h1>{{$post->title}}</h1>
    <h2>{{$post->description}}</h2>
    <p>{{$post->content}}</p>
</div>
@endsection
