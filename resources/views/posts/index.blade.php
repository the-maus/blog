@extends('base')
@section('content')
<div class="container">
    <h1>Listagem de posts</h1>
    <div class="row">
        @foreach($posts as $post)
        <div class="col-md-4">
            <div class="card mb-4 shadow-sm">
                <div class="card-body">
                    <p class="card-text">{{$post->title}}</p>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="btn-group">
                            <a href="{{route('posts.show', $post->id)}}" class="btn btn-sm btn-outline-secondary">Visualizar</a>
                            <a href="{{route('posts.edit', $post->id)}}" class="btn btn-sm btn-outline-secondary">Editar</a>
                            <form action="{{route('posts.destroy', $post->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-outline-secondary">Remover</button>
                            </form>
                        </div>
                        <small class="text-muted">9 mins</small>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
