@extends('base')
@section('content')
<div class="container">
    <form action="{{route('posts.store')}}" method="post">
        @csrf
        <label for="title">Título</label>
        <input type="text" name="title" id="title" class="form-control">
        <label for="description">Descrição</label>
        <input type="text" name="description" id="description" class="form-control">
        <label for="content">Conteúdo</label>
        <textarea name="content" id="content" class="form-control"></textarea>
        <br>
        <input type="submit" value="Salvar" class="btn btn-primary" style="float: right">
    </form>
</div>
@endsection
